#!/bin/bash

set -e 

react-scripts build 
sed -i 's/\],/\,\/\^\\\/blog\/\]\,/' build/service-worker.js || true
echo 'self.addEventListener("install", (event) => self.skipWaiting())' >> build/service-worker.js
