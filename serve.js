const fs = require('fs')
const https = require('https')
const express = require('express')
const path = require('path')
const app = express()

const DIRECTORY = process.env.SERVE_DIRECTORY || 'build'
const PORT = process.env.SERVER_PORT || 8443
const COLORS = {
  YELLOW: '\x1b[33m%s\x1b[0m'
}
app.use(express.static(DIRECTORY))

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, DIRECTORY, 'index.html'))
})

const options = {
  key: fs.readFileSync('key.pem', 'utf8'),
  cert: fs.readFileSync('cert.pem', 'utf8'),
  passphrase: process.env.HTTPS_PASSPHRASE || ''
}
const server = https.createServer(options, app)

server.listen(PORT, () => {
  console.info(`  Serving static files from ${DIRECTORY} folder on: \n\r`)
  console.info(COLORS.YELLOW, `   https://localhost:${PORT}`)
})
