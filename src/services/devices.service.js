/*
 * Copyright (c) 2017-2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import last from 'lodash.last'
import merge from 'lodash.merge'
import sortBy from 'lodash.sortby'
import dayjs from 'dayjs'
import {
  _getJSON,
  _delete,
  _patchJSON,
  _putJSON,
  _postJSON,
  _requestContentType,
  API_URL
} from './api.service'

import { throwErrorIf, AUTH_ERRORS } from '../lib/api.helpers'
import { resolvePath } from '../lib/object.helpers'

const DEVS_URL = `${API_URL}/devices`
const SUMMARY_URL = `${API_URL}/trails/summary`

const trailPostUrl = (deviceId) =>
  `${API_URL}/trails/${deviceId}/steps/`

const trailObjectsURL = (id, rev) =>
  `${API_URL}/trails/${id}/steps/${rev}/objects`

const tailsDeviceSummaryUrl = (id) =>
  `${API_URL}/trails/${id}/summary`

const devLogsURL = (id, after, page = 500, sort = '-time-created') =>
  `${API_URL}/logs/?dev=prn:::devices:/${id}&after=${after}&page=${page}&sort=${sort}`

const devStepsURL = (id, status = '') =>
  `${API_URL}/trails/${id}/steps?progress.status=${status}`

const keyBlacklist = ['#spec']
const expandableExtensions = ['json']

function isUploaded (summaryResponse) {
  return resolvePath(summaryResponse, 'json.state-sha', '').length > 0
}

// Device API
export const getDevsData = async token => _getJSON(SUMMARY_URL, token)

export const getDeviceData = async (token, id) =>
  _getJSON(`${DEVS_URL}/${id}`, token)

export const getDeviceSummary = async (token, id) =>
  _getJSON(tailsDeviceSummaryUrl(id), token)

export const getDeviceSteps = async (token, id, status) =>
  _getJSON(devStepsURL(id, status), token)

export const getDeviceObjects = async (token, id, rev) =>
  _getJSON(trailObjectsURL(id, rev), token)

export const getDeviceLogs = async (
  token,
  id,
  after = '1970-01-01T00:00:00.0Z',
  page = 2000,
  sort = '-time-created'
) => _getJSON(devLogsURL(id, after, page, sort), token)

export const deleteDevice = async (token, id) =>
  _delete(`${DEVS_URL}/${id}`, token)

export const publishDevice = async (token, id) =>
  _putJSON(`${DEVS_URL}/${id}/public`, token)

export const unPublishDevice = async (token, id) =>
  _delete(`${DEVS_URL}/${id}/public`, token)

export const claimDevice = async (token, id, secret) =>
  _putJSON(`${DEVS_URL}/${id}?challenge=${secret}`, token)

export const setDeviceMetadata = async (token, id, meta, type = 'user-meta') =>
  _putJSON(`${DEVS_URL}/${id}/${type}`, token, meta)

export const fetchFile = async (token, url) =>
  _requestContentType(url, token, 'text/plain; charset=utf-8')

export const patchDevice = async (token, id, payload) =>
  _patchJSON(`${DEVS_URL}/${id}`, token, payload)

export const postTrails = async (token, id, payload) =>
  _postJSON(trailPostUrl(id), token, payload)

async function getLogsData (token, deviceId, fromRefresher) {
  const logsResponse = await getDeviceLogs(
    token,
    deviceId,
    fromRefresher ? window.__PH_CDLLETS : undefined,
    fromRefresher
      ? parseInt(process.env.REACT_APP_LOGS_WARM_CHUNK) || 500
      : parseInt(process.env.REACT_APP_LOGS_COLD_CHUNK) || 1000,
    fromRefresher ? 'time-created' : '-time-created'
  )

  throwErrorIf(logsResponse)(AUTH_ERRORS)

  return resolvePath(logsResponse, 'json.entries', [])
}

async function getAllDataFromApi (args) {
  const { token, deviceId, device, refresh, revision } = args
  let devResponse = {}
  let stepsResponse = {}
  let summaryResponse = {}
  let stateObjectsResponse = {}
  if (!refresh) {
    return {
      dev: device,
      steps: device.history.steps
    }
  }

  // fetch device information and enrich it
  [devResponse, summaryResponse] = await Promise.all([
    getDeviceData(token, deviceId),
    getDeviceSummary(token, deviceId)
  ])

  const rev = revision || resolvePath(summaryResponse, 'json.progress-revision')

  throwErrorIf(devResponse, summaryResponse)(AUTH_ERRORS)

  // Only Load steps and logs if device upload all files
  if (isUploaded(summaryResponse)) {
    [stepsResponse, stateObjectsResponse] = await Promise.all([
      getDeviceSteps(token, deviceId, '{"$ne":""}'),
      getDeviceObjects(token, deviceId, rev)
    ])
  }

  throwErrorIf(stepsResponse, stateObjectsResponse)(AUTH_ERRORS)

  return {
    dev: devResponse.json,
    summary: summaryResponse.json,
    steps: resolvePath(stepsResponse, 'json', []),
    objects: resolvePath(stateObjectsResponse, 'json', [])
  }
}

function getRevisions (steps = [], revision) {
  const lastRevision = resolvePath(steps, `${steps.length - 1}.rev`, 0)
  const parsedRevision = parseInt(revision, 10)
  const rev = isNaN(parsedRevision) ? lastRevision : parsedRevision
  const revIndex = steps.findIndex(x => x.rev === rev)
  return {
    lastRevision,
    rev,
    revIndex
  }
}

export function buildCurrentStep (steps, objects = [], revIndex) {
  const currentStep = Object.assign({}, resolvePath(steps, `${revIndex}`, {}))

  currentStep.rawState = merge({}, currentStep.state)

  currentStep.state = Object.assign(
    {},
    Object.entries(currentStep.state || {}).reduce((acc, [key, value]) => {
      const extension = last((key || '').split('.'))
      const isExpandable = expandableExtensions.indexOf(extension) >= 0
      acc[key] = {
        extension,
        isExpandable,
        hidden: keyBlacklist.indexOf(key) !== -1,
        value
      }
      return acc
    }, {})
  )

  // TODO: implement an objects cache to only request the new or missing objects here
  // or to completely omit this request if there were no changes in files between revisions
  const stateURLs = objects.reduce((acc, object) => {
    // FIXME: use env var here
    acc[object.id] = `${object['signed-geturl']}`
    return acc
  }, {})

  // update current step state with download urls
  currentStep.state = Object.assign(
    {},
    Object.entries(currentStep.state).reduce((acc, [key, spec]) => {
      acc[key] = {
        url: stateURLs[spec.value] || null,
        ...spec
      }
      return acc
    }, {})
  )

  return currentStep
}

function consolidateDeviceData (args) {
  const { dev, lastRevision, steps, rev, revIndex, currentStep, summary } = args
  return Object.assign({}, dev, {
    summary,
    history: {
      revision: rev,
      lastRevision,
      steps,
      revIndex,
      currentStep
    }
  })
}

export async function getDeviceLogsService (token, deviceId, fromRefresher) {
  const newLogs = await getLogsData(token, deviceId, fromRefresher)

  if (!fromRefresher) {
    newLogs.reverse()
  }

  const devInitNewLogs = sortBy(newLogs || [], 'tsec')
  const devInitNewestLogEntry = devInitNewLogs.slice(-1)
  const devInitNewestLogEntryTime = (devInitNewestLogEntry.length === 1
    ? dayjs(devInitNewestLogEntry[0]['time-created'])
    : dayjs(window.__PH_CDLLETS)
  ).toISOString()

  window.__PH_CDLLETS = devInitNewestLogEntryTime

  return {
    newLogs: devInitNewLogs,
    devInitNewestLogEntryTime
  }
}

export async function getAllDeviceData (args) {
  const {
    revision
  } = args
  const {
    dev,
    steps,
    summary,
    objects
  } = await getAllDataFromApi(args)
  const { lastRevision, rev, revIndex } = getRevisions(steps, revision)
  const currentStep = buildCurrentStep(steps, objects, revIndex)
  const consolidatedDevice = consolidateDeviceData({
    dev,
    lastRevision,
    rev,
    revIndex,
    currentStep,
    steps,
    summary
  })
  return {
    consolidatedDevice
  }
}
