/*
 * Copyright (c) 2017-2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {
  _getJSON,
  _postJSON,
  API_URL
} from './api.service'

const AUTH_STATUS = `${API_URL}/auth/auth_status`
const LOGIN_URL = `${API_URL}/auth/login`
const ACCOUNTS_URL = `${API_URL}/auth/accounts`
const OAUTH2_AUTHORIZE_URL = `${API_URL}/auth/authorize`
const OAUTH2_CODE_URL = `${API_URL}/auth/code`
const AUTH_PASSWORD_RECOVER = `${API_URL}/auth/recover`
const AUTH_PASSWORD_URL = `${API_URL}/auth/password`
const verifyAccountURL = (id, challenge) =>
  `${API_URL}/auth/verify?id=${id}&challenge=${challenge}`

// Helpers
const getOauth2AuthorizeUrlByType = (type) => {
  switch (type) {
    case 'code':
      return OAUTH2_CODE_URL
    default:
      return OAUTH2_AUTHORIZE_URL
  }
}

// Auth API
export const login = async (username, password) =>
  _postJSON(LOGIN_URL, '', { username, password })

export const register = async (nick, email, password, captchaToken, encrypted) =>
  _postJSON(ACCOUNTS_URL, '', { nick, email, password, captcha: captchaToken, 'encrypted-account': encrypted })

export const verifyAccount = async (id, challenge) =>
  _getJSON(verifyAccountURL(id, challenge))

export const getUserData = async (token) =>
  _getJSON(AUTH_STATUS, token)

export const requestPasswordRecover = async (email) =>
  _postJSON(AUTH_PASSWORD_RECOVER, undefined, { email })

export const postPassword = async (token, password) =>
  _postJSON(AUTH_PASSWORD_URL, undefined, { token, password })

export const postOauth2Authorize = async (service, scopes, redirectUri, state, token, responseType) =>
  _postJSON(getOauth2AuthorizeUrlByType(responseType), token, {
    service,
    scopes,
    state,
    'redirect_uri': redirectUri,
    'response_type': responseType
  })
