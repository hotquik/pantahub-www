/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/
import { claimDevice } from '../../services/devices.service'
import { logout } from '../auth/actions'

import * as Types from './types'
import { resolvePath } from '../../lib/object.helpers'

const emptyZeroConf = {
  watch () {
    console.info('Zero Conf can be loaded')
  },
  AUTH_SET_PASSWORD () {
    console.info('Zero Conf can be loaded')
  }
}

function Zeroconf () {
  // eslint-disable-next-line no-undef
  return cordova && cordova.plugins && cordova.plugins.zeroconf
    // eslint-disable-next-line no-undef
    ? cordova.plugins.zeroconf
    : emptyZeroConf
}

export const deviceClaimSetDeviceID = id => ({
  type: Types.DEV_CLAIM_SET_DEVICEID,
  id
})

export const deviceClaimSetSecret = secret => ({
  type: Types.DEV_CLAIM_SET_SECRET,
  secret
})

export const deviceClaimInProgress = id => ({
  type: Types.DEV_CLAIM_INPROGR,
  id
})

export const deviceClaimSuccess = (id, response) => ({
  type: Types.DEV_CLAIM_SUCCESS,
  id,
  response
})

export const deviceClaimFailure = (id, error) => ({
  type: Types.DEV_CLAIM_FAILURE,
  id,
  error
})

export const deviceClaim = (token, id, secret) => async (dispatch, getState) => {
  dispatch(deviceClaimInProgress(id))
  if (token === null) {
    token = resolvePath(getState(), 'auth.token')
  }
  try {
    const resp = await claimDevice(token, id, secret)

    if (!resp.ok) {
      dispatch(deviceClaimFailure(id, resp.json))
      if (resp.status === 401) dispatch(logout())
    } else dispatch(deviceClaimSuccess(id, resp.json))
  } catch (err) {
    dispatch(deviceClaimFailure(id, { code: 0, message: err }))
  }
}

export const toggleNetworkDevices = () => ({
  type: Types.TOGGLE_WATCH_NETWORK_DEVICES
})

export const addDevice = (device) => ({
  type: Types.ADD_DEVICE,
  payload: device
})

export const removeDevice = (device) => ({
  type: Types.REMOVE_DEVICE,
  payload: device
})

export const toggleManual = () => ({
  type: Types.TOGGLE_MANUAL
})

export const unwatchForNetworkDevices = () => async (dispatch) => {
  dispatch(toggleNetworkDevices())
  Zeroconf().stop()
}

export const watchForNetworkDevices = () => async (dispatch, getState) => {
  const { claims: { watching = false } } = getState()
  if (watching) {
    return
  }

  dispatch(toggleNetworkDevices())
  Zeroconf().registerAddressFamily = 'ipv4'
  Zeroconf().watchAddressFamily = 'ipv4'
  Zeroconf().watch('_ssh._tcp.', 'local.', processZero(dispatch))
}

const processZero = (dispatch) => (result) => {
  console.info(result)
  if (
    resolvePath(result, 'action') === 'resolved' &&
    (resolvePath(result, 'service.ipv4Addresses.0', null) !== null ||
    resolvePath(result, 'service.ipv6Addresses.0', null) !== null)
  ) {
    dispatch(addDevice(result.service))
  }
}
