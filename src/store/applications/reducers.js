/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'
import { buildBasicReducers } from '../../lib/redux.helpers'
// import { resolvePath } from '../../lib/object.helpers'

export const STATES = {
  IN_PROGRESS: 'inprogr',
  FAILURE: 'failure',
  SUCCESS: 'success'
}

export const DIRECTION = {
  ASC: 1,
  DESC: -1
}

export const initialState = {
  status: STATES.IN_PROGRESS,
  error: undefined,
  sortField: undefined,
  sortDirection: DIRECTION.DESC,
  apps: [],
  currentApp: {
    id: '',
    type: '',
    nick: '',
    prn: '',
    secret: null,
    'time-created': undefined,
    'time-modified': undefined,
    scopes: [],
    exposed_scopes: [],
    redirect_uris: []
  },
  scopes: []
}

const mergeCurrentApp = (state, action, successPrefix) => ({
  ...state[successPrefix],
  ...action.payload
})

const reducer = {
  ...buildBasicReducers(STATES, Types, 'APP_CREATE', 'currentApp', undefined, mergeCurrentApp),
  ...buildBasicReducers(STATES, Types, 'APP_UPDATE', 'currentApp', undefined, mergeCurrentApp),
  ...buildBasicReducers(STATES, Types, 'APP_DELETE', 'currentApp'),
  ...buildBasicReducers(STATES, Types, 'APP_GET', 'currentApp', undefined, mergeCurrentApp),
  ...buildBasicReducers(STATES, Types, 'APP_GET_ALL', 'apps'),
  ...buildBasicReducers(STATES, Types, 'APP_GET_SCOPES', 'scopes'),
  [Types.APP_SET]: (state, action) => ({
    ...state,
    currentApp: {
      ...state.currentApp,
      ...action.payload
    }
  }),
  [Types.APP_SET_SORT]: (state, action) => {
    const newDirection = state.sortDirection * -1
    return {
      ...state,
      sortField: action.field,
      sortDirection: newDirection,
      apps: state.apps.sort((a, b) => {
        if (a[action.field] > b[action.field]) {
          return -1 * newDirection
        }
        if (a[action.field] < b[action.field]) {
          return 1 * newDirection
        }
        return 0
      })
    }
  },
  default: (state, action) => state
}

export const reduceFunction = (state = initialState, action) => {
  const r = reducer[action.type] || reducer.default
  return r(state, action)
}
