/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React from 'react'

import { CopyToClipboard } from 'react-copy-to-clipboard'

export const CopyButton = props => (
  <CopyToClipboard text={props.value}>
    <button
      className={`clipboard-button ${props.className}`}
      title={props.title}
      type="button"
      disabled={props.disabled}
    >
      {props.children}
    </button>
  </CopyToClipboard>
)

export const TextInputWithClipboard = props => (
  <div className="input-group input-group-sm">
    <input
      type="text"
      className="form-control form-control-sm"
      value={props.value}
      aria-label={props.label}
      disabled
    />
    <span className="input-group-append">
      <CopyButton
        className="btn btn-sm btn-light"
        value={props.value}
        title={props.title || 'Copy Value'}
        disabled={props.disabled}
      >
        <i className="mdi mdi-content-copy" aria-hidden="true" />
      </CopyButton>
    </span>
  </div>
)

export const JSONDisplayWithClipboard = props => {
  const jsonStr = JSON.stringify(props.value, null, 2)
  return (
    <div className="json-with-clipboard">
      <CopyButton
        className="btn btn-sm btn-light"
        value={jsonStr}
        title={props.title || 'Copy JSON contents'}
        disabled={props.disabled}
      >
        <i className="mdi mdi-content-copy" aria-hidden="true" />
      </CopyButton>
      <div className="json-value-wrapper">
        <pre>{jsonStr}</pre>
      </div>
    </div>
  )
}
