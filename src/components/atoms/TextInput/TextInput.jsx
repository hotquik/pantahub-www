import React from 'react'

export default function TextInput (props) {
  const {
    name,
    label,
    onInput = () => {},
    onChange = () => {},
    error,
    type = 'text',
    ...rest
  } = props

  if (!label) {
    return (
      <React.Fragment>
        <input
          type={type}
          id={name}
          name={name}
          onInput={onInput}
          onChange={onChange}
          {...rest}
        />
        <div className="invalid-feedback"> {error} </div>
      </React.Fragment>
    )
  }

  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <input
        type={type}
        id={name}
        name={name}
        onInput={onInput}
        onChange={onChange}
        {...rest}
      />
      <div className="invalid-feedback"> {error} </div>
    </div>
  )
}
