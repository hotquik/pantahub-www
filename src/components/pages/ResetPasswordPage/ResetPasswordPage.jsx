/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useEffect, useState, useCallback } from 'react'
import CreateNewPassword from '../../organisms/PasswordRecovery/CreateNewPassword'
import { parseImplicitData } from '../../../lib/api.helpers'

export default function ResetPasswordPage ({ location }) {
  const [token, setToken] = useState()
  const parseData = useCallback(parseImplicitData)

  useEffect(() => {
    setToken(parseData(location.hash).token)
  }, [parseData, location])

  return (
    <section className="container">
      <section className="rpp-page col-sm-12 d-flex flex-column justify-content-center align-items-center">
        <header className="rpp-page__header col-sm-6 text-center">
          <h1>Set you new password</h1>
        </header>
        <CreateNewPassword token={token} />
      </section>
    </section>
  )
}
