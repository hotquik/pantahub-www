/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { clearError } from '../../../store/general-errors/actions'
import { HTTP_STATUS } from '../../../lib/api.helpers'
import './general_error.scss'

function ErrorMessage ({ type = 'error', message, dismissError }) {
  const errorClass = `general-errors general-errors--${type}`

  useEffect(() => {
    setTimeout(() => { dismissError() }, 2000)
  }, [dismissError])

  return (
    <div className={errorClass}>
      <div className="general-errors__wrapper">
        <span className="general-errors__message">
          {message}
        </span>
        <button className="general-errors__close btn btn-link" onClick={dismissError}>
          <i className="mdi mdi-close-box"></i>
        </button>
      </div>
    </div>
  )
}

function GeneralError ({ errors, dismissError }) {
  if (errors.type === HTTP_STATUS.FORBIDDEN) {
    return (
      <ErrorMessage
        message="Your don't have permissions to this resource"
        dismissError={dismissError}
      />
    )
  }
  return null
}

export default connect(
  state => ({ errors: state.errors }),
  dispatch => ({
    dismissError: (event) => {
      if (event && event.preventDefault) {
        event.preventDefault()
      }
      dispatch(clearError())
    }
  })
)(GeneralError)
