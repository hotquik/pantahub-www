/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'

import { connect } from 'react-redux'

import { Link } from 'react-router-dom'

import { initializeDashboard } from '../../../store/dashboard/actions'

import { userDashboardPath } from '../../../router/routes'
import UserDashboardSubscriptionCharts from '../UserDashboard/UserDashboardSubscriptionCharts'
import UserDashboardTopDevices from '../UserDashboard/UserDashboardTopDevices'

class UserAccountInfo extends Component {
  render () {
    const { username } = this.props
    return (
      <div className="user-account-info">
        <div className="jumbotron">
          <div className="row">
            <div className="col-sm-5">
              <div className="user-image" />
              <h3>Ayram Alberto Huitlacoche</h3>
              <p className="text-muted">{username}</p>
            </div>
            <div className="col-sm-7">
              <p>
                <strong>Tier:</strong> Beta{' '}
                <Link to={`${userDashboardPath}/${username}`}>[Upgrade]</Link>
              </p>
              {/*
              <p>
                <strong>Location:</strong> Mexico City, Mexico
              </p>
              <p>
                <strong>Member Since:</strong> 10/02/2017
              </p>
              <strong>Address</strong>
              <address>
                Av. Ejército Nacional 276.<br />
                Dpto. 202. Colonia Anzures.<br />
                Del. Miguel Hidalgo<br />
                Mexico City, 11590<br />
                Mexico
              </address>
              <button className="btn btn-dark">Edit Profile & Billing</button> */}
            </div>
          </div>
          <br />
        </div>
      </div>
    )
  }
}

const UserAccountQuotaChartsMessage = () => (
  <div>
    <p>
      <strong>Need more power?</strong>
    </p>
    <p>Upgrade to a Pro Plan today!</p>
    <Link to={userDashboardPath}>Upgrade Now and Get 2 Months Free</Link>
  </div>
)

class UserAccountQuotaCharts extends Component {
  render () {
    const { subscription, loading } = this.props
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-8">
            <div className="quota-box">
              <h4>Your Quota</h4>
              <UserDashboardSubscriptionCharts
                subscription={subscription}
                loading={loading}
              />
            </div>
            <br />
          </div>
          <div className="col-md-4 bg-light">
            <div className="row">
              <div className="col-2" />
              <div className="col-10">
                <UserAccountQuotaChartsMessage />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

class UserAccountTopDevices extends Component {
  render () {
    const { username, topDevices, loading } = this.props
    return (
      <div className="container">
        <div className="row">
          <div className="col-12 table-top-devices">
            <h4>Your Top Devices</h4>
            <UserDashboardTopDevices
              topDevices={topDevices}
              username={username}
              loading={loading}
            />
          </div>
        </div>
      </div>
    )
  }
}

class UserAccount extends Component {
  componentDidMount () {
    const { init, auth } = this.props
    const { token } = auth

    init(token)
  }

  render () {
    const { auth, dash } = this.props
    const { username } = auth
    const { data, loading } = dash
    const topDevices = data['top-devices'] || []
    const subscription = data['subscription'] || {}

    return (
      <React.Fragment>
        <div
          key="breadcrumbs"
          className="breadcrumb-sector row align-items-center"
        >
          <div className="col-12">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to={`${userDashboardPath}/${username}`}>Dashboard</Link>
              </li>
              <li className="breadcrumb-item active">Account</li>
            </ol>
          </div>
        </div>,
        <UserAccountInfo key="userAccountInfo" username={username} />,
        <UserAccountQuotaCharts
          subscription={subscription}
          loading={loading}
          username={username}
        />,
        <UserAccountTopDevices
          username={username}
          topDevices={topDevices}
          loading={loading}
        />
      </React.Fragment>
    )
  }
}

export default connect(
  state => state,
  dispatch => ({
    init: token => {
      dispatch(initializeDashboard(token))
    }
  })
)(UserAccount)
