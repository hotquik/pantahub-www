/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { Link } from 'react-router-dom'

import TruncatedText from '../../atoms/TruncatedText/TruncatedText'
import DeviceDeleteButton from '../../molecules/DeviceDeleteButton/DeviceDeleteButton'
import DevicePublishButton from '../../molecules/DevicePublishButton/DevicePublishButton'
import RelativeTime from '../../atoms/RelativeTime/RelativeTime'

import { CopyButton } from '../../atoms/ClipboardFields/ClipboardFields'
import { classForDevStatus } from '../../../lib/utils'
import { DeviceStatus, DeviceStatusMessage } from '../UserDevice/DeviceStatus'
import { userDashboardPath } from '../../../router/routes'

export default function DeviceRow ({ dev, token, username, deleteTracker, publishTracker }) {
  return (
    <tr className="device-nick">
      <td>
        <div className={`status-led bg-${classForDevStatus(dev.status)}`} />{' '}
        <Link
          to={`${userDashboardPath}/${username}/devices/${dev.deviceid}`}
        >
          <TruncatedText text={dev['device-nick']} />
          {dev.public && (
            <i
              title="This device is public"
              className="mdi mdi-earth"
              aria-hidden="true"
            />
          )}{' '}
        </Link>
      </td>
      <td className="progress-revision text-monospace">
        {dev['state-sha'] ? (
          <React.Fragment>
            <span className="progress-revision-commit">
              <TruncatedText text={dev['state-sha']} size={8} />
            </span>
            {' '}
            <span className="progress-revision-rev text-muted">
                  ({dev['progress-revision']})
            </span>
          </React.Fragment>
        ) : (
          <span className="progress-revision-rev">
            {dev['progress-revision']}
          </span>
        )}
      </td>
      <td className="step-time">
        <RelativeTime when={dev['step-time']} />
      </td>
      <td className="timestamp">
        <RelativeTime when={dev['timestamp']} />
      </td>
      <td className="status">
        <DeviceStatus status={dev.status} statusMsg={dev['status-msg']} />
      </td>
      <td className="status-msg">
        <DeviceStatusMessage status={dev.status} statusMsg={dev['status-msg']} truncated={true} size={14} />
      </td>
      <td className="text-right actions">
        <div className="btn-group" role="group" aria-label="Device actions">
          <DevicePublishButton
            token={token}
            deviceId={dev.deviceid}
            isPublic={dev.public}
            loading={(publishTracker[dev.deviceid] || {}).loading}
            compact
          />
          <CopyButton
            className="btn btn-sm btn-light"
            title="Copy Share URL"
            value={`https://www.pantahub.com/${username}/${
              dev.deviceid
            }/step/${dev['progress-revision']}`}
          >
            <i className="mdi mdi-link" aria-hidden="true" />
          </CopyButton>
          <DeviceDeleteButton
            username={username}
            token={token}
            deviceId={dev.deviceid}
            compact
            loading={(deleteTracker[dev.deviceid] || {}).loading}
            redirectTo={null}
          />
        </div>
      </td>
    </tr>
  )
}
