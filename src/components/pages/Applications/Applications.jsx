/*
 * Copyright (c) 2017-2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'

import './thridpartyapp.scss'
import { connect } from 'react-redux'
import { getApps, deleteAppAndReload, toggleSorting } from '../../../store/applications/actions'
import { Link } from 'react-router-dom'
import { userDashboardPath } from '../../../router/routes'
import AppList from '../../organisms/AppList/AppList'

class Application extends Component {
  componentDidMount () {
    this.props.getApps()
  }

  render () {
    return (
      <div className="thrid-app">
        <div
          key="breadcrumbs"
          className="breadcrumb-sector row align-items-center"
        >
          <div className="col-8">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to={`${userDashboardPath}/user`}>Dashboard</Link>
              </li>
              <li className="breadcrumb-item active">Applications</li>
            </ol>
          </div>
        </div>
        <div className="col-12 text-right mb-4">
          <Link
            to={`${userDashboardPath}/username/thridpartyapps/edit/new`}
            className="btn btn-primary"
          >
            Create new app
          </Link>
        </div>
        <section>
          <AppList deleteApp={this.props.deleteApp} toggleSorting={this.props.toggleSorting} {...this.props.applications} />
        </section>
      </div>
    )
  }
}

export default connect(
  state => ({
    applications: state.applications
  }),
  {
    getApps,
    toggleSorting,
    deleteApp: deleteAppAndReload
  }
)(Application)
