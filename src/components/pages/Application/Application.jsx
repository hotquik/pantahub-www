/*
 * Copyright (c) 2017-2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { getApp } from '../../../store/applications/actions'
import { userDashboardPath } from '../../../router/routes'

class Application extends Component {
  async componentDidMount () {
    await this.props.getApp(this.props.match.params.id)
  }

  render () {
    const app = this.props.applications.currentApp
    return (
      <div className="thridpartyapp-detail">
        <div
          key="breadcrumbs"
          className="breadcrumb-sector row align-items-center"
        >
          <div className="col-8">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to={`${userDashboardPath}/user`}>Dashboard</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to={`${userDashboardPath}/user/thridpartyapps/`}>Applications</Link>
              </li>
              <li className="breadcrumb-item active">{app.nick}</li>
            </ol>
          </div>
        </div>
        <div className="row align-items-center">
          <div className="col-sm-10">
            <h1>
              {app.nick}
            </h1>
          </div>
          <div className="col-sm-2 text-right">
            <Link
              to={`${userDashboardPath}/username/thridpartyapps/edit/${app.id}`}
              className="btn btn-primary"
            >
              Edit
            </Link>
          </div>
        </div>
        <div className="row mt-4">
          <div className="col-md-8">
            <p><b>ID:</b> {app.id}</p>
            <p><b>Name:</b> {app.prn}</p>
            <p><b>Type:</b> {app.type}</p>
            {app.secret && (<p><b>Secret:</b> {app.secret}</p>)}
            <p>
              <b>Callback URLs:</b>
            </p>
            <ul>
              {app.redirect_uris.map((url, id) => (
                <li
                  key={id}
                >
                  {url}
                </li>
              ))}
            </ul>
            <p>
              <b>Scopes:</b>
            </p>
            <ul>
              {app.scopes.sort(s => s.required).map(scope => (
                <li
                  key={`${scope.service}${scope.id}`}
                >
                  {scope.service}/{scope.id}{' '}
                  {scope.description && (<span>({scope.description})</span>)}{' '}
                  {scope.required && (<b>required</b>)}
                </li>
              ))}
            </ul>
            {app.type === 'confidential' && app.exposed_scopes.length > 0 && (
              <React.Fragment>
                <p>
                  <b>Exposed Scopes:</b>
                </p>
                <ul>
                  {app.exposed_scopes.map(scope => (
                    <li
                      key={`${scope.service}${scope.id}`}
                    >
                      {scope.service}/{scope.id}{' '}
                      {scope.description && (<span>({scope.description})</span>)}{' '}
                      {scope.required && (<b>required</b>)}
                    </li>
                  ))}
                </ul>
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  state => ({
    applications: state.applications
  }),
  {
    getApp
  }
)(Application)
