/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import merge from 'lodash.merge'
import news from '../../../data/news.json'

export default class UserDashboardNews extends Component {
  state = {
    toggledEntries: {}
  };

  _toggleEntry (idx) {
    const isToggled = this.state.toggledEntries[idx]
    this.setState(
      merge({}, this.state, {
        toggledEntries: {
          [idx]: !isToggled
        }
      })
    )
  }

  render () {
    const { className } = this.props
    const { toggledEntries } = this.state

    return (
      <div className={`feed-dashboard ${className || ''}`}>
        <h2>Latest News</h2>
        <ul className="list-feed list-unstyled">
          {news.slice(0, 5).map((entry, idx) => (
            <li key={idx}>
              <i className="mdi mdi-chevron-right" aria-hidden="true" />
              <span className="badge badge-light">{entry.date}</span>
              {' '}
              <button
                className="btn btn-link"
                onClick={() => this._toggleEntry.bind(this)(idx)}
              >
                {entry.title}
              </button>
              {toggledEntries[idx] && (
                <React.Fragment>
                  <p>{entry.content}</p>
                  <p>
                    {entry.content2}{' '}
                    <a href={entry.link_url}>{entry.link_text}</a>
                  </p>
                </React.Fragment>
              )}
            </li>
          ))}
        </ul>
      </div>
    )
  }
}
