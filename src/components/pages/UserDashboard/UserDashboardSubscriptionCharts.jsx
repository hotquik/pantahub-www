/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import Loading from '../../atoms/Loading/Loading'

const UserDashboardQuotaDoughnut = React.lazy(() => import('./UserDashboardQuotaDoughnut'))

export default class UserDashboardSubscriptionCharts extends Component {
  render () {
    const { subscription, loading } = this.props

    const quotaStats = (subscription || {})['quota-stats'] || {}

    const bandwidth = quotaStats['BANDWIDTH'] || {}
    const actualBandwidth = bandwidth['Actual'] || 0
    const maxBandwidth = bandwidth['Max'] || 0
    const billingPeriod = quotaStats['BILLINGPERIOD'] || {}
    const daysLeft = (billingPeriod['Max'] || 0) - (billingPeriod['Actual'] || 0)
    const objects = quotaStats['OBJECTS'] || {}
    const actualObjects = objects['Actual'] || 0
    const maxObjects = objects['Max'] || 0
    const devices = quotaStats['DEVICES'] || {}
    const actualDeviceCount = devices['Actual'] || 0
    const maxDeviceCount = devices['Max'] || 0

    const quotasWidget = loading ? (
      <Loading />
    ) : (
      <React.Suspense fallback={<Loading />}>
        <div className="row">
          <div className="col-4">
            <div className="chart-bar bandwidth-bar text-center">
              <UserDashboardQuotaDoughnut
                actual={actualBandwidth}
                max={maxBandwidth}
              />
              <strong>Bandwidth</strong>
              <br />
              <span className="quota-detail">
                {actualBandwidth} of {maxBandwidth} GB
              </span>
              <br />
              <span className="small text-muted">{daysLeft} Days Left</span>
              <br />
            </div>
          </div>
          <div className="col-4">
            <div className="chart-bar billing-bar text-center">
              <UserDashboardQuotaDoughnut
                actual={actualObjects}
                max={maxObjects}
              />
              <strong>Storage</strong>
              <br />
              <span className="quota-detail">
                {actualObjects} of {maxObjects} GB
              </span>
              <br />
            </div>
          </div>
          <div className="col-4">
            <div className="chart-bar bandwidth-bar text-center">
              <UserDashboardQuotaDoughnut
                actual={actualDeviceCount}
                max={maxDeviceCount}
              />
              <strong>Devices</strong>
              <br />
              <span className="quota-detail">
                {actualDeviceCount} of {maxDeviceCount}
              </span>
              <br />
            </div>
          </div>
        </div>
      </React.Suspense>
    )

    return quotasWidget
  }
}
