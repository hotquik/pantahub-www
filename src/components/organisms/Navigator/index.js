/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { PureComponent } from 'react'

import UserDeviceNavigatorReadme from './Readme'
import UserDeviceNavigatorFiles from './Files'
import UserDeviceNavigatorJSON from './JSON'
import UserDeviceNavigatorMetadata from './EditableMetadata'
import UserDeviceMetadata from './Metada'
import UserDeviceNavigatorHistory from './History'
import UserDeviceNavigatorConsole from './Console'
import Tiles from '../../molecules/Tiles/Tiles'

function DeviceMetaDataInfo (props) {
  return (
    <UserDeviceMetadata type="device-meta" flatten={true} {...props} />
  )
}

const tabs = [
  {
    id: 'readme',
    label: 'Readme',
    components: [UserDeviceNavigatorReadme]
  },
  {
    id: 'apps',
    label: 'Apps',
    components: [Tiles]
  },
  {
    id: 'files',
    label: 'Files',
    components: [UserDeviceNavigatorFiles]
  },
  {
    id: 'json',
    label: 'JSON',
    components: [UserDeviceNavigatorJSON]
  },
  {
    id: 'metadata',
    label: 'Metadata',
    components: [
      DeviceMetaDataInfo,
      UserDeviceNavigatorMetadata
    ]
  },
  {
    id: 'history',
    label: 'History',
    components: [UserDeviceNavigatorHistory]
  },
  {
    id: 'console',
    label: 'Console',
    components: [UserDeviceNavigatorConsole]
  }
]

class UserDeviceNavigator extends PureComponent {
  render () {
    const {
      onChangeNavigatorTab,
      navigator,
      activeTab,
      device,
      username,
      token,
      dispatch
    } = this.props

    return (
      <div className="device-navigator">
        <header>
          <h3>Device Navigator</h3>
        </header>
        <div className="row">
          <div className="col-md-12">
            <ul
              className="nav nav-tabs nav-fill flex-column flex-sm-row"
              role="tablist"
            >
              {tabs.map(tab => (
                <li className="nav-item" key={tab.id}>
                  <button
                    className={`btn btn-link btn-block nav-link ${
                      tab.id === activeTab ? 'active' : ''
                    }`}
                    id={`tab-${tab.id}`}
                    onClick={onChangeNavigatorTab(tab.id)}
                    data-toggle="tab"
                    role="tab"
                    aria-controls={`tab-${tab.id}`}
                    aria-expanded={tab.id === activeTab}
                  >
                    {tab.label}
                  </button>
                </li>
              ))}
            </ul>

            <div className="tab-content">
              {tabs.map(tab => (
                <div
                  key={tab.id}
                  className={`tab-pane fade show ${
                    tab.id === activeTab ? 'active' : ''
                  }`}
                  id={`tab-content-${tab.id}`}
                  role="tabpanel"
                  aria-labelledby={`tab-${tab.id}`}
                >
                  {tab.components.map((TabComponent, key) => {
                    return (
                      <TabComponent
                        key={key}
                        device={device}
                        username={username}
                        token={token}
                        dispatch={dispatch}
                        navigator={navigator}
                      />
                    )
                  })}
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default UserDeviceNavigator
