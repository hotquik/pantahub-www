/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import merge from 'lodash.merge'
import sortBy from 'lodash.sortby'

import { JSONDisplayWithClipboard } from '../../atoms/ClipboardFields/ClipboardFields'
import { resolvePath } from '../../../lib/object.helpers'

class UserDeviceNavigatorFiles extends Component {
  state = {
    toggledKeys: {}
  };

  toggleKey (key) {
    const { toggledKeys } = this.state
    const value = !toggledKeys[key]

    this.setState(
      merge({}, this.state, {
        toggledKeys: {
          [key]: value
        }
      })
    )
  }

  render () {
    const { toggledKeys } = this.state
    const { device, disabled } = this.props

    const { state, rawState } = resolvePath(device, 'history.currentStep', {})

    return (
      <table className="table table-hover table-responsive-sm table-no-first-border">
        <tbody>
          {sortBy(Object.entries(state || {}), [
            pair => !pair[1].isExpandable,
            pair => pair[0]
          ])
            .filter(([key, { hidden }]) => key && !hidden)
            .map(([key, { isExpandable, value, url }]) => {
              const expanded =
                toggledKeys[key] === undefined ? false : toggledKeys[key]
              const actionButton = isExpandable ? (
                <button
                  className="btn btn-sm btn-outline-dark"
                  onClick={() => this.toggleKey(key)}
                  title={expanded ? 'Hide contents' : 'Show contents'}
                  disabled={disabled}
                >
                  <i
                    className={`mdi mdi-chevron-${expanded ? 'up' : 'down'}`}
                    aria-hidden="true"
                  />
                </button>
              ) : (
                <a
                  href={url || ''}
                  target="_blank"
                  rel="noopener noreferrer"
                  title="Click to save"
                  disabled={disabled}
                  className="btn btn-sm btn-outline-dark"
                >
                  <span className="mdi mdi-download" aria-hidden="true" />
                </a>
              )

              let rows = [
                <tr key={key}>
                  <td>
                    {key}
                    {isExpandable ? (
                      ''
                    ) : (
                      <span className="badge badge-secondary" title={value}>
                        SHA256
                      </span>
                    )}
                  </td>
                  <td className="text-right">{actionButton}</td>
                </tr>
              ]

              if (isExpandable && expanded) {
                rows.push(
                  <tr className="expander" key={`${key}-expander`}>
                    <td colSpan={2}>
                      <JSONDisplayWithClipboard
                        value={rawState[key]}
                        disabled={disabled}
                      />
                    </td>
                  </tr>
                )
              }

              return rows
            })}
        </tbody>
      </table>
    )
  }
}

export default UserDeviceNavigatorFiles
