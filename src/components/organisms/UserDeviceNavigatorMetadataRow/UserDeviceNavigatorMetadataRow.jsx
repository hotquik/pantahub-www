import React, { useState, memo } from 'react'

export default memo(function UserDeviceNavigatorMetadataRow (props) {
  const [state, setState] = useState({
    inEdit: false,
    newKey: props.currentKey,
    newValue: props.value
  })
  const {
    inProgress,
    loading,
    disabled,
    currentKey: key,
    value,
    onEdit,
    saveHandler,
    onDelete
  } = props
  const { inEdit, newKey, newValue } = state
  return (
    <tr
      className={`${
        inProgress || (disabled && !inEdit) ? 'table-secondary' : ''
      } ${inEdit ? 'metadata-edit new-entry-form' : ''}`}
      title={`${inProgress ? 'Operation in progress' : ''}`}
    >
      <td className="meta-key">
        {inEdit ? (
          <input
            className="form-control my-1 mr-sm-2"
            type="text"
            placeholder={key}
            value={newKey}
            onChange={evt => {
              setState({
                ...state, newKey: evt.target.value
              })
            }}
          />
        ) : (
          <pre>{`${key}`}</pre>
        )}
      </td>
      <td>
        {inEdit ? (
          <textarea
            className="form-control my-1 mr-sm-2 textarea"
            aria-label="Metadata Field Value"
            placeholder={value}
            value={newValue}
            rows={Math.max(newValue.split('\n').length, 1)}
            onChange={evt => {
              setState({
                ...state, newValue: evt.target.value
              })
            }}
          />
        ) : (
          <pre>{`${value}`}</pre>
        )}
      </td>
      <td className="actions" width="150">
        <div className="btn-group my-1 mr-sm-2">
          {inEdit ? (
            <React.Fragment>
              <button
                style={{ color: 'white' }}
                className={`btn btn-sm btn-primary ${
                  inProgress || loading ? 'disabled' : ''
                }`}
                disabled={inProgress || loading}
                title="Save"
                onClick={() => {
                  saveHandler(key, newKey, value, newValue)
                  setState({
                    ...state,
                    inEdit: false
                  })
                }}
              >
                <span
                  className={`mdi ${
                    inProgress ? 'mdi-sync-alert' : 'mdi-content-save'
                  }`}
                />
              </button>
              <button
                href={undefined}
                style={{ color: 'white' }}
                className={`btn btn-sm btn-info ${
                  inProgress || loading ? 'disabled' : ''
                }`}
                disabled={inProgress || loading}
                title="Cancel"
                onClick={() => {
                  setState({
                    ...state,
                    inEdit: false,
                    newKey: props.key,
                    newValue: props.value
                  })
                  onEdit(false)
                }}
              >
                <span
                  className={`mdi ${
                    inProgress ? 'mdi-sync-alert' : 'mdi-cancel'
                  }`}
                />
              </button>
            </React.Fragment>
          ) : (
            <button
              style={{ color: 'white' }}
              className={`btn btn-sm btn-info ${
                inProgress || loading || disabled ? 'disabled' : ''
              }`}
              disabled={inProgress || loading || disabled}
              title="Edit this key"
              onClick={() => {
                setState({
                  ...state,
                  inEdit: true
                })
                onEdit(true)
              }}
            >
              <span
                className={`mdi ${
                  inProgress ? 'mdi-sync-alert' : 'mdi-pencil'
                }`}
              />
            </button>
          )}
          <button
            style={{ color: 'white' }}
            className={`btn btn-sm btn-danger ${
              inProgress || loading || disabled ? 'disabled' : ''
            }`}
            disabled={inProgress || loading || disabled}
            title="Remove this key from metadata"
            onClick={() => {
              onDelete(key)
            }}
          >
            <span
              className={`mdi ${
                inProgress ? 'mdi-sync-alert' : 'mdi-delete'
              }`}
            />
          </button>
        </div>
      </td>
    </tr>
  )
})
