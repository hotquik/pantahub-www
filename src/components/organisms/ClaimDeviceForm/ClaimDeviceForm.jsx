/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'

import { connect } from 'react-redux'

import { Link } from 'react-router-dom'

import Loading from '../../atoms/Loading/Loading'

import {
  deviceClaim,
  deviceClaimSetDeviceID,
  deviceClaimSetSecret
} from '../../../store/claims/actions'

import { userDashboardPath } from '../../../router/routes'

class ClaimDeviceForm extends Component {
  render () {
    const {
      onChangeDeviceId,
      onChangeSecret,
      onClaimDevice,
      auth,
      claims
    } = this.props
    const { username, token } = auth
    const { device, secret, loading, error, response } = claims

    const disabled = loading || !device || !secret
    return (
      <section className="deviceClaimForm">
        {response !== null && error === null ? (
          <p>
            Your device has been successfully claimed and you can now
            access it on{' '}
            <Link to={`${userDashboardPath}/${username}/devices`}>
              Your Devices
            </Link>.
          </p>
        ) : (
          <p>
            Please provide the ID and Challenge for the device you want
            to claim.
          </p>
        )}
        {error !== null && (
          <p className="alert alert-danger">
            {error['Error'] ||
              (typeof error['message'] === 'string' &&
                error['message']) ||
              JSON.stringify(error)}
          </p>
        )}
        {loading ? (
          <Loading />
        ) : (
          response === null && (
            <form onSubmit={onClaimDevice(token, device, secret)}>
              <div className="form-group">
                <label htmlFor="username">Device ID</label>
                <input
                  type="text"
                  autoCapitalize="none"
                  className="form-control"
                  onChange={onChangeDeviceId}
                  placeholder="Device ID"
                />
              </div>
              <div className="form-group">
                <label htmlFor="username">Challenge</label>
                <input
                  type="text"
                  autoCapitalize="none"
                  className="form-control"
                  onChange={onChangeSecret}
                  placeholder="your-device-secret"
                />
              </div>
              <button
                type="submit"
                disabled={disabled}
                className="btn btn-dark btn-lg btn-block"
                title={
                  disabled
                    ? loading
                      ? 'Claiming Device'
                      : 'Please provide the required details'
                    : 'Claim Device'
                }
              >
                Claim
              </button>
            </form>
          )
        )}
      </section>
    )
  }
}

export default connect(
  state => state,
  dispatch => ({
    onChangeDeviceId: evt => dispatch(deviceClaimSetDeviceID(evt.target.value)),
    onChangeSecret: evt => dispatch(deviceClaimSetSecret(evt.target.value)),
    onClaimDevice: (token, id, secret) => async evt => {
      evt.preventDefault()
      await dispatch(deviceClaim(token, id, secret))
    }
  })
)(ClaimDeviceForm)
