import React, { useRef } from 'react'
import { connect } from 'react-redux'

import TextInput from '../../atoms/TextInput/TextInput'
import useFormValidation from '../../../hooks/useFormValidation'
import { requestPasswordEmail, resetErrorOnPasswordEmail } from '../../../store/auth/actions'

import './request_new_password.scss'

export function RequestNewPassword ({ loading = false, error = null, email = '', done, onSave, resetError }) {
  const {
    formState,
    onSubmit,
    onInput,
    getError
  } = useFormValidation()

  const formRef = useRef()

  const submit = (event) => {
    event.preventDefault()
    const { isValid, data } = onSubmit(event)
    if (isValid) {
      onSave(data.email)
    }
  }
  const baseClass = 'rnp-form'
  const wasValidatedClass = formState.wasValidated ? `${baseClass} was-validated` : baseClass
  const onInputWrapper = (event) => {
    resetError()
    onInput(event)
  }
  return (
    <section className="rnp col-sm-6">
      <form
        onSubmit={submit}
        noValidate={true}
        className={wasValidatedClass}
        ref={formRef}
      >
        <div className="form-row justify-content-center">
          <div className="col-12 rnp__email">
            <TextInput
              className="form-control"
              name="email"
              type="email"
              placeholder="youremail@domain.com"
              required={true}
              error={getError('email')}
              onInput={onInputWrapper}
              defaultValue={email}
            />
          </div>
        </div>
        <div className="form-row pt-2 justify-content-center">
          <div className="col-12 text-center">
            <div className="text-danger"> {error} </div>
            {done && (<div className="text-success"> {"You will soon receive an e-mail containing a link to reset your password. If you can't find it, please also check your junk mail."} </div>)}
          </div>
          <div className="col-auto">
            <button
              type="submit"
              className="btn btn-dark rnp__submit"
              disabled={loading || !!error || !formState.isValid}
            >
              {loading && (<span className="mdi mdi-refresh pantahub-loading" />)}
              Send it
            </button>
          </div>
        </div>
      </form>
    </section>
  )
}

export default connect(
  state => ({
    ...state.auth.requestPassword
  }),
  {
    onSave: requestPasswordEmail,
    resetError: resetErrorOnPasswordEmail
  }
)(RequestNewPassword)
