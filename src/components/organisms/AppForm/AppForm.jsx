import React, { useCallback, useState, useEffect } from 'react'
import { STATES, initialState } from '../../../store/applications/reducers'
import { validate } from './validation'
import TextInput from '../../atoms/TextInput/TextInput'
import AddFromList from '../../molecules/AddFromList/AddFromList'
import SelectInput from '../../atoms/SelectInput/SelectInput'
import AddList from '../../molecules/AddList/AddList'

const ownScopesFields = [
  { name: 'id', label: 'Name', type: 'text', className: 'col-3' },
  { name: 'description', label: 'Description', type: 'text', className: 'col-8' }
]

export default function AppForm ({ currentApp, updateApp, status, scopes }) {
  const [state, setState] = useState({ ...initialState.currentApp, wasValidated: false, errors: [] })

  useEffect(() => {
    setState(s => ({
      ...s,
      ...currentApp
    }))
  }, [currentApp])

  const onChange = (value) => (event) => {
    const input = event.target.value
    setState({
      ...state,
      [value]: input
    })
  }

  const submit = useCallback((event) => {
    event.preventDefault()
    const form = validate(state)
    if (form.valid) {
      updateApp(state, state.id)
      return
    }
    const errors = Object.values(form.errors).reduce((acc, v) => [...acc, ...v], [])
    setState({ ...state, errors: errors })
  }, [state, updateApp])

  const updateScope = (id, scope) => { state.scopes[id] = scope; setState(state) }
  const removeScope = id => { setState({ ...state, scopes: [...state.scopes.slice(0, id), ...state.scopes.slice(id + 1)] }) }

  const loading = status === STATES.IN_PROGRESS
  const wasValidatedClass = state.wasValidated ? 'col-md-8 was-validated' : 'col-md-8'
  const saveCreateMessage = state.id ? 'Save app' : 'Create app'

  return (
    <div className="app-form row justify-content-center">
      <form
        onSubmit={submit}
        noValidate={true}
        className={wasValidatedClass}
      >
        <TextInput
          label="App nick"
          name="nick"
          type="text"
          placeholder="Application Name"
          className="form-control"
          value={state.nick}
          onChange={onChange('nick')}
        />
        <SelectInput
          label="App type"
          name="type"
          className="form-control"
          options={[{ value: '', label: 'Select One' }, { value: 'public', label: 'Public' }, { value: 'confidential', label: 'Confidential' }]}
          value={state.type}
          onChange={onChange('type')}
        />
        <hr/>
        <AddFromList
          values={state.scopes}
          optionsMap={{ getLabel: (o) => `${o.description} (${o.id})` }}
          onAdd={(v) => setState({ ...state, scopes: [...state.scopes, v] })}
          onUpdate={updateScope}
          onRemove={removeScope}
          getId={(v) => `${v.service}/${v.id}`}
          options={scopes}
          label="Reading scopes"
          name="scopes"
        />
        {state.type === 'confidential' && (
          <React.Fragment>
            <hr/>
            <AddList
              fields={ownScopesFields}
              onAdd={(v) => setState(s => ({ ...state, exposed_scopes: [...state.exposed_scopes, v] }))}
              onUpdate={(v) => setState(s => ({ ...state, exposed_scopes: v }))}
              onRemove={id => setState({ ...state, exposed_scopes: [...state.exposed_scopes.slice(0, id), ...state.exposed_scopes.slice(id + 1)] })}
              values={state.exposed_scopes}
              label="Exposed Scopes"
            />
          </React.Fragment>
        )}
        <hr/>
        <AddList
          fields={[{ name: null, label: 'URL', placeholder: 'New callback URL', type: 'text', className: 'col-11' }]}
          onAdd={(v) => { setState({ ...state, redirect_uris: [...state.redirect_uris, v['null']] }) }}
          onUpdate={v => setState({ ...state, redirect_uris: v })}
          onRemove={id => setState({ ...state, redirect_uris: [...state.redirect_uris.slice(0, id), ...state.redirect_uris.slice(id + 1)] })}
          values={state.redirect_uris}
          label="Callback whitelist"
        />
        { state.errors.length > 0 && (
          <ul>
            {state.errors.map((e, i) => (
              <li key={`${i}-${e.type}`}>{e.message}</li>
            ))}
          </ul>
        )}
        <button
          type="submit"
          className="btn btn-dark btn-lg btn-block mt-4"
          disabled={loading}
        >
          {loading ? (
            <span className="mdi mdi-refresh pantahub-loading" />
          ) : (
            saveCreateMessage
          )}
        </button>
      </form>
    </div>
  )
}
