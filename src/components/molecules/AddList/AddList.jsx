import React, { useState } from 'react'

import './add-list.scss'

export default function AddList ({ onAdd, onUpdate, onRemove, values, fields, name, label }) {
  const [state, setState] = useState({})

  const updateLocal = (field) => (event) => {
    const newValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value
    setState(s => ({ ...s, [field]: newValue }))
  }

  const onChange = (id, field) => (event) => {
    const newValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value
    if (!field) {
      values[id] = newValue
    } else {
      values[id][field] = newValue
    }
    onUpdate(values)
  }

  const addNew = (event) => {
    event.preventDefault()
    onAdd(state)
    setState({})
  }

  const remove = (id) => (event) => {
    event.preventDefault()
    onRemove(id)
  }

  return (
    <div className="col-12">
      <label htmlFor={name}>{label}</label>
      <section className="table table-hover add-list">
        <header className="add-list__header row">
          {fields.map((f, id) => (
            <div key={id} className={f.className}>{f.label}</div>
          ))}
          <div className="">
            Actions
          </div>
        </header>
        <section className="add-list__body">
          {values.map((v, i) => (
            <div key={i} className="row align-items-center" >
              {fields.map((f, j) => (
                <div className={f.className} key={j}>
                  <input
                    name={`${f.name}-${f.id}`}
                    id={`${f.name}-${f.id}`}
                    type={`${f.type}`}
                    value={v[f.name] || v || ''}
                    onChange={onChange(i, f.name)}
                  />
                </div>
              ))}
              <div className="">
                <div className="row justify-content-md-center">
                  <button className="btn btn-link" onClick={remove(i)} >
                    <i className="mdi mdi-delete-forever" />
                  </button>
                </div>
              </div>
            </div>
          ))}
          <div className="row align-items-center">
            {fields.map((f, j) => (
              <div className={f.className} key={j}>
                <input
                  type={`${f.type}`}
                  value={state[f.name] || ''}
                  onChange={updateLocal(f.name)}
                  placeholder={f.placeholder}
                />
              </div>
            ))}
            <div className="">
              <div className="row justify-content-md-center">
                <a href="#addElement" className="btn btn-link" onClick={addNew}>
                  <i className="mdi mdi-plus" />
                </a>
              </div>
            </div>
          </div>
        </section>
      </section>
    </div>
  )
}
