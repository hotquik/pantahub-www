import React, { useRef } from 'react'
import isEqual from 'lodash.isequal'

import './add-form-list.scss'

const defaultOptionsMap = { getValue: (o) => o, getLabel: (o) => o }

function Inner (props) {
  const {
    name,
    label,
    onAdd = () => {},
    onRemove = () => {},
    onUpdate = () => {},
    getId = (v) => v,
    options = [],
    values = [],
    optionsMap = {}
  } = props

  const option = useRef()
  const removeId = (id) => (event) => {
    event.preventDefault()
    onRemove(id)
  }
  const updatePart = (id, part) => (event) => {
    const newValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value
    onUpdate(id, { ...values[id], [part]: newValue })
  }
  const addOption = (event) => {
    event.preventDefault()
    if (!values.some(v => isEqual(getId(v), getId(options[option.current.value])))) {
      onAdd(options[option.current.value])
    }
  }
  const mapper = {
    ...defaultOptionsMap,
    ...optionsMap
  }

  return (
    <div className="add-form-list">
      <div className="header form-row">
        <label htmlFor={name}>{label}</label>
        <select
          id={name}
          name={name}
          ref={option}
          className="form-control col-11"
        >
          {options.map((o, id) => (
            <option key={id} value={id}>{mapper.getLabel(o)}</option>
          ))}
        </select>
        <button
          className="btn btn-link col-1"

        >
          <i onClick={addOption} className="mdi mdi-plus"/>
        </button>
      </div>
      { values.length > 0 && (
        <div className="body">
          <div className="list-header row">
            <div className="col-9 text-left name">
              <b>Name</b>
            </div>
            <div className="col-2 text-center default">
              <b>Required</b>
            </div>
            <div className="col-1 text-center actions">{' '}</div>
          </div>
          {values.map((v, id) => (
            <div key={id} className="header row align-items-center">
              <div className="col-9">
                {v.description} ({v.service}/{v.id})
              </div>
              <div className="col-2 text-center">
                <input
                  type="checkbox"
                  id={`required$-${id}`}
                  className="form-checkbox"
                  defaultChecked={v.required}
                  onChange={updatePart(id, 'required')}
                />
              </div>
              <div className="col-1">
                <a href="#removeScope" onClick={removeId(id)} className="text-center">
                  <i className="mdi mdi-delete-forever" />
                </a>
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  )
}

export default function AddFromList (props) {
  const {
    label = '',
    name
  } = props
  return label > 0 ? (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <Inner {...props} />
    </div>
  ) : (
    <React.Fragment>
      <Inner {...props} />
    </React.Fragment>
  )
}
