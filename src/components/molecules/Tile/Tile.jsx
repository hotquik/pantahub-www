import React, { useState, Fragment, useCallback } from 'react'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import isEmpty from 'lodash.isempty'
import { resolvePath } from '../../../lib/object.helpers'

export const EMPTY_SOURCE = {
  '#spec': '',
  args: {
    PV_DEBUG_MODE: '',
    PV_SECURITY_FULLDEV: ''
  },
  config: {
    Entrypoint: ''
  },
  docker_digest: 'sha:',
  docker_name: '',
  docker_tag: '',
  persistence: {
    'lxc-overlay': ''
  },
  template: '',
  title: ''
}

const sourceImageUrl = (image, tag = '') => {
  if (!image) {
    return ''
  }
  const imageUrl = image.split('/')
  return `${imageUrl[imageUrl.length - 1]}:${tag}`
}

const shortSha = (sha = 'sha:') => sha.split(':')[1].slice(0, 7)

function SourceSummary ({ data }) {
  const value = resolvePath(data, 'value.full', data.value)
  const summary = resolvePath(data, 'value.summary', data.value)

  if (value === ':' || value === 'sha:' || value === '') {
    return null
  }

  if (resolvePath(data, 'value.summary', '') === '') {
    return (
      <div>
        <b>{data.key}: </b> {value}
      </div>
    )
  }
  return (
    <div>
      <OverlayTrigger
        placement="top"
        key={data.key}
        trigger={['click', 'focus']}
        overlay={
          <Popover id={`tooltip-${data.key}`}>
            <Popover.Content>
              {value}
            </Popover.Content>
          </Popover>
        }
      >
        <div className="overlay-trigger-pointer">
          <b>{data.key}: </b> {summary}
        </div>
      </OverlayTrigger>
    </div>
  )
}

function SourceGroupDetail ({ args = {}, title, size }) {
  const argsArray = Object.keys(args).reduce((acc, key) => {
    return isEmpty(args[key])
      ? acc
      : [...acc, { key: key, value: args[key] }]
  }, [])

  if (argsArray.length === 0) {
    return null
  }

  return (
    <div className="tile__arguments">
      {title && (<h6>{title}</h6>)}
      <ul className={`list-group list-group-flush ${size}`}>
        {argsArray.map(a => (
          <li key={a.key} className="list-group-item">
            <SourceSummary data={a} />
          </li>
        ))}
      </ul>
    </div>
  )
}

export default function Tile ({ source = EMPTY_SOURCE, expansable = false }) {
  const [open, setOpen] = useState(false)

  const togleDetail = useCallback(() => {
    setOpen(!open && expansable)
  }, [expansable, open])

  return (
    <section className="tile card">
      <div className="card-header">
        {source.title}
      </div>
      <div className="card-body">
        <SourceGroupDetail
          args={{
            'Image': {
              summary: sourceImageUrl(source.docker_name, source.docker_tag),
              full: `${source.docker_name}:${source.docker_tag}`
            },
            'Version': {
              summary: shortSha(source.docker_digest),
              full: source.docker_digest
            },
            'Command': resolvePath(source, 'config.Entrypoint', undefined)
          }}
        />
        {open && (
          <Fragment>
            <SourceGroupDetail args={source.volumes} title="Volumes" size="small" />
            <SourceGroupDetail args={source.args} title="Environment" size="small" />
          </Fragment>
        )}
      </div>
      {expansable && (
        <div className="card-footer text-muted align-items-center" onClick={togleDetail}>
          More details {open ? (<i className="mdi mdi-chevron-up"></i>) : (<i className="mdi mdi-chevron-down"></i>)}
        </div>
      )}
    </section>
  )
}
