/*
 * Copyright (c) 2017, 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'

import { Link } from 'react-router-dom'

import { tosPath } from '../../../router/routes'

import dayjs from 'dayjs'

import './footer.scss'

class Footer extends Component {
  render () {
    const changeset = process.env.REACT_APP_REVISION || null

    return (
      <div className="footer-container">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <ul className="nav nav-footer float-left">
                <li className="nav-item">
                  <a className="nav-link" target="_blank" rel="noopener noreferrer" href="http://gitlab.com/pantacor">
                    Git
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/#contact_us">
                    Contact Us
                  </a>
                </li>
                <li className="nav-item">
                  <Link to={tosPath} className="nav-link">
                    Disclaimer
                  </Link>
                </li>
                <li className="nav-item">
                  <a className="nav-link" target="_blank" rel="noopener noreferrer" href="https://www.pantahub.com/privacy">
                    Privacy Policy
                  </a>
                </li>
              </ul>
              <span className="float-right copyright-box">
                © 2017, {dayjs().format('YYYY')} Pantacor Ltd.
                {changeset ? `| ${changeset}` : ''}
              </span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Footer
