/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { resolvePath } from '../../../lib/object.helpers'
import { ClAIM_STATUSES } from '../../../store/claims/reducer'

import './unclaimed.scss'
import { connect } from 'react-redux'
import { deviceClaim } from '../../../store/claims/actions'

function claimingText (device) {
  switch (device.status) {
    case ClAIM_STATUSES.IN_PROGRESS:
      return 'Claiming device'
    case ClAIM_STATUSES.FAILURE:
      return 'Try again'
    case ClAIM_STATUSES.SUCCESS:
      return 'Device claimed'
    default:
      return 'Claim device'
  }
}

export function UnClaimedDevice ({ device, deviceClaim }) {
  const onClaim = (event) => {
    if (event && event.preventDefault) {
      event.preventDefault()
    }
    deviceClaim(null, device.id, device.secret)
  }
  return (
    <section className="unclaimed-device">
      <ul>
        <li>
          <span className="label">ID</span>: {device.id}
        </li>
        {device.host && (
          <li>
            <span className="label">Host</span>: {device.host}
          </li>
        )}
        {resolvePath(device, 'ipv4Addresses.0') && (
          <li>
            <span className="label">IPv4</span>: {resolvePath(device, 'ipv4Addresses', []).join(', ')}
          </li>
        )}
        {resolvePath(device, 'ipv6Addresses.0') && (
          <li>
            <span className="label">IPv6</span>: {resolvePath(device, 'ipv6Addresses', []).join(', ')}
          </li>
        )}
        {device.port && (
          <li>
            <span className="label">Port</span>: {device.port}
          </li>
        )}
      </ul>
      <div className="actions text-right">
        <button
          type="submit"
          disabled={device.status === ClAIM_STATUSES.IN_PROGRESS || device.status === ClAIM_STATUSES.SUCCESS }
          className="btn btn-primary btn-small"
          onClick={onClaim}
          title={claimingText(device)}
        >
          {claimingText(device)}
        </button>
      </div>
    </section>
  )
}

export default connect(
  null,
  {
    deviceClaim
  }
)(UnClaimedDevice)
