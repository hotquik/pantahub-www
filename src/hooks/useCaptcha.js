import { useRef, useState } from 'react'

export default function useCaptcha () {
  const captchaRef = useRef()
  const [captchaToken, setCaptchaToken] = useState()

  const onErroredCallback = () => {
    if (captchaRef.current) {
      captchaRef.current.reset()
    }
  }

  const verifyCallback = function () {
    if (!captchaRef.current) {
      return
    }
    const recaptchaValue = captchaRef.current.getValue()
    setCaptchaToken(recaptchaValue)
  }

  const expiredCallback = () => {
    if (captchaRef.current) {
      captchaRef.current.reset()
    }
    setCaptchaToken(false)
  }

  return {
    captchaRef,
    captchaToken,
    onErroredCallback,
    verifyCallback,
    expiredCallback
  }
}
