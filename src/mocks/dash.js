export const dash = {
  'prn': 'prn:::accounts:/5c8f93dceea823000876c4fb',
  'nick': 'testuser',
  'subscription': {
    'plan-id': 'FREE',
    'billing': {
      'Type': 'Monthly',
      'AmountDue': 0,
      'Currency': 'USD',
      'VatRegion': 'World'
    },
    'quota-stats': {
      'BANDWIDTH': {
        'Name': 'BANDWIDTH',
        'Actual': 0,
        'Max': 2,
        'Unit': 'GiB'
      },
      'DEVICES': {
        'Name': 'DEVICES',
        'Actual': 2,
        'Max': 25,
        'Unit': 'Piece'
      },
      'OBJECTS': {
        'Name': 'OBJECTS',
        'Actual': 0.86,
        'Max': 2,
        'Unit': 'GiB'
      }
    }
  },
  'top-devices': [
    {
      'device-id': '5cc33bdcef3139000b7358ed',
      'nick': 'fleet-house_group_004-20191956-3619756825565427642662794731609630067355615191870114961237971044333958611852',
      'prn': 'prn:::devices:/5cc33bdcef3139000b7358ed',
      'message': 'Device changed at 2019-05-10 14:18:04.242 +0000 UTC',
      'type': 'INFO',
      'status': 'DONE',
      'last-activity': '2019-05-10T14:18:21.722Z'
    },
    {
      'device-id': '5cc33b1037966d0008f18375',
      'nick': 'horribly_finer_dove',
      'prn': 'prn:::devices:/5cc33b1037966d0008f18375',
      'message': 'Device changed at 2019-05-10 14:18:07.428 +0000 UTC',
      'type': 'INFO',
      'status': 'NEW',
      'last-activity': '2019-05-10T14:18:07.428Z'
    }
  ]
}
